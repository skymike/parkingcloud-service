 DROP TABLE IF EXISTS t_order_detail;
 CREATE TABLE t_order_detail (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   order_id varchar(32) NOT NULL,
   address varchar(100) DEFAULT NULL,
   create_time mediumtext,
   finish_time mediumtext,
   description varchar(255) DEFAULT NULL,
   car_id varchar(32) DEFAULT NULL,
   price double(10,2) DEFAULT NULL,
   parkinglot_id varchar(32) DEFAULT NULL,
   phone varchar(13) DEFAULT NULL,
   parkinglot_detail_id int DEFAULT NULL
 );

 DROP TABLE IF EXISTS t_order_status;
 CREATE TABLE t_order_status (
   id int NOT NULL PRIMARY KEY,
   name varchar(32) NOT NULL
 );

 DROP TABLE IF EXISTS t_parkinglot;
 CREATE TABLE t_parkinglot (
   parkinglot_id varchar(32) NOT NULL PRIMARY KEY,
   name varchar(100) NOT NULL,
   address varchar(255) DEFAULT NULL,
   size int DEFAULT NULL,
   current_size int DEFAULT NULL,
   is_opened tinyint(1) DEFAULT NULL,
   create_time mediumtext,
   unit_price double DEFAULT '5'
 );

 DROP TABLE IF EXISTS t_parkinglot_detail;
 CREATE TABLE t_parkinglot_detail (
   id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
   parkinglot_id varchar(32) NOT NULL,
   area_code varchar(32) DEFAULT NULL,
   seat_id varchar(32) DEFAULT NULL,
   status tinyint(1) DEFAULT NULL,
   create_time mediumtext
 );

 DROP TABLE IF EXISTS t_role;
 CREATE TABLE t_role (
   id int NOT NULL PRIMARY KEY,
   name varchar(32) DEFAULT NULL,
   description varchar(255) DEFAULT NULL
 );

 DROP TABLE IF EXISTS t_user;
 CREATE TABLE t_user (
   id varchar(32) NOT NULL PRIMARY KEY,
   nick_name varchar(32) DEFAULT NULL,
   password varchar(32) DEFAULT NULL,
   create_time mediumtext,
   last_login_time mediumtext,
   status tinyint(1) DEFAULT NULL,
   email varchar(45) DEFAULT NULL,
   phone varchar(13) DEFAULT NULL
 );

 DROP TABLE IF EXISTS t_user_car;
 CREATE TABLE t_user_car (
   id int NOT NULL PRIMARY KEY,
   uid varchar(32) NOT NULL,
   car_id varchar(32) DEFAULT NULL,
   is_default tinyint(1) DEFAULT NULL
 );

 DROP TABLE IF EXISTS t_user_order;
 CREATE TABLE t_user_order (
   order_id varchar(32) NOT NULL PRIMARY KEYk,
   parkinglot_id varchar(32) DEFAULT NULL,
   car_id varchar(32) DEFAULT NULL,
   user_id varchar(32) DEFAULT NULL,
   status_id int DEFAULT '1',
   create_time mediumtext COMMENT '订单创建时间',
   start_time mediumtext COMMENT '订单预约起始时间',
   finish_time mediumtext COMMENT '订单完成时间',
   description varchar(255) DEFAULT NULL,
   price double(10,2) DEFAULT NULL,
   end_time mediumtext COMMENT '订单预约结束时间'
 );

 DROP TABLE IF EXISTS t_user_role;
 CREATE TABLE t_user_role (
   uid varchar(32) NOT NULL,
   role_id int NOT NULL,
   PRIMARY KEY (`uid`,`role_id`)
 );