package com.oocl.parkingcloudservice.unittest;

import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.impl.UserRegisterServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
public class UserRegisterServiceTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private UserRegisterServiceImpl userRegisterService;

  @Test
  void should_return_userEmail_code_when_register_success_given_one_user() {
    //given
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail("1298472398@qq.com");
    userEntity.setStatus(true);
    userEntity.setNickName("Kevin");
    userEntity.setPassword("123456");
    userEntity.setPhone("986224565825");
    //when
    Mockito.when(userRepository.save(any())).thenReturn(userEntity);
    UserEntity result = userRegisterService.registerUser(userEntity);
    //then
    assertEquals(userEntity.getEmail(), result.getEmail());
  }

  @Test
  void should_return_null_when_register_false_given_null_email() {
    //given
    UserEntity userEntity = new UserEntity();
    userEntity.setStatus(true);
    userEntity.setEmail("1298472398@qq.com");
    userEntity.setNickName("Kevin");
    userEntity.setPassword("123456");
    userEntity.setPhone("986224565825");
    //when
    List<UserEntity> userEntityList = new ArrayList<>();
    userEntityList.add(userEntity);
    Mockito.when(userRepository.findAll()).thenReturn(userEntityList);
    UserEntity result = userRegisterService.registerUser(userEntity);
    //then
    assertNull(result);
  }
}
