package com.oocl.parkingcloudservice.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.oocl.parkingcloudservice.entity.UserCarEntity;
import com.oocl.parkingcloudservice.repository.UserCarRepository;
import com.oocl.parkingcloudservice.service.impl.UserCarServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserCarServiceTest {

  @Mock
  private UserCarRepository userCarRepository;
  @InjectMocks
  private UserCarServiceImpl userCarService;

  @Test
  void should_return_size_0_when_find_user_car_by_user_id_given_without_car_own_by_user() {
    //given
    Mockito.when(userCarRepository.findByUserId("default"))
        .thenReturn(new ArrayList<UserCarEntity>());
    //when
    List<UserCarEntity> userCars = userCarService.findUserCarByUserId("default");
    //then
    assertEquals(0, userCars.size());
  }

  @Test
  void should_return_2_when_find_user_car_by_user_id_given_user_own_2_car() {
    //given
    ArrayList<UserCarEntity> userCarEntities = new ArrayList<>();
    userCarEntities.add(new UserCarEntity());
    userCarEntities.add(new UserCarEntity());
    Mockito.when(userCarRepository.findByUserId("default")).thenReturn(userCarEntities);
    //when
    List<UserCarEntity> userCars = userCarService.findUserCarByUserId("default");
    //then
    assertEquals(2, userCars.size());
  }

  @Test
  void shoulad_return_car_id_2_when_find_default_user_car_by_user_id_given_default_car_id_2() {
    //given
    ArrayList<UserCarEntity> userCarEntities = new ArrayList<>();
    UserCarEntity userCarEntity1 = new UserCarEntity();
    userCarEntity1.setDefault(false);
    userCarEntity1.setCarId("2");
    userCarEntities.add(userCarEntity1);
    UserCarEntity userCarEntity = new UserCarEntity();
    userCarEntity.setCarId("2");
    userCarEntity.setDefault(true);
    userCarEntities.add(userCarEntity);
    Mockito.when(userCarRepository.findByUserId("default")).thenReturn(userCarEntities);
    //when
    UserCarEntity userCar = userCarService.findDefaultUserCarByUserId("default");
    //then
    assertEquals("2", userCar.getCarId());
  }
}
