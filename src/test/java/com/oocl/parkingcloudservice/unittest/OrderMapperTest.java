package com.oocl.parkingcloudservice.unittest;

import com.oocl.parkingcloudservice.dto.OrderResponse;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import com.oocl.parkingcloudservice.mapper.OrderMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OrderMapperTest {
  @Test
  void should_return_order_response_when_entity_to_response() {
    //given
    UserOrderEntity userOrderEntity = new UserOrderEntity();
    userOrderEntity.setId("7981465714020651008");
    userOrderEntity.setCarId("A001");
    userOrderEntity.setStartTime(1597413600000L);
    userOrderEntity.setEndTime(1597935600000L);
    userOrderEntity.setPrice(5.00);
    userOrderEntity.setCreateTime(1597159621L);
    userOrderEntity.setStatusId(200);
    ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
    parkingLotEntity.setName("zhuhai parking lot");
    OrderResponse orderResponse = OrderMapper.entityToResponse(userOrderEntity,parkingLotEntity);
    System.out.println(orderResponse.toString());
    Assertions.assertEquals(parkingLotEntity.getName(),orderResponse.getParkingLotName());
    Assertions.assertEquals(userOrderEntity.getCreateTime(),orderResponse.getCreateTime());
    Assertions.assertEquals(userOrderEntity.getStartTime(),orderResponse.getStartTime());
    Assertions.assertEquals(userOrderEntity.getEndTime(),orderResponse.getEndTime());
    Assertions.assertEquals(userOrderEntity.getId(),orderResponse.getOrderId());
    Assertions.assertEquals(userOrderEntity.getStatusId(),orderResponse.getStatusId());
    Assertions.assertEquals(userOrderEntity.getPrice(),orderResponse.getPrice());
  }
}
