package com.oocl.parkingcloudservice.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.oocl.parkingcloudservice.entity.OrderDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.ReserveInfoEntity;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import com.oocl.parkingcloudservice.repository.OrderDetailRepository;
import com.oocl.parkingcloudservice.repository.ParkingLotDetailRepository;
import com.oocl.parkingcloudservice.repository.ParkingLotRepository;
import com.oocl.parkingcloudservice.repository.UserOrderRepository;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.impl.ParkingLotServiceImpl;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ParkingLotServiceTest {

  @Mock
  private ParkingLotDetailRepository parkingLotDetailRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private OrderDetailRepository orderDetailRepository;

  @Mock
  private UserOrderRepository userOrderRepository;

  @Mock
  private ParkingLotRepository parkingLotRepository;

  @InjectMocks
  private ParkingLotServiceImpl parkingLotService;

  @Test
  void should_return_2_seats_when_get_seats_by_parking_lot_1001_given_2_seats_in_parking_lot_1001() {
    //given
    ParkingLotDetailEntity entity1 = new ParkingLotDetailEntity();
    ParkingLotDetailEntity entity2 = new ParkingLotDetailEntity();
    List<ParkingLotDetailEntity> entities = Arrays.asList(entity1, entity2);
    when(parkingLotDetailRepository.findByParkingLotId("1001")).thenReturn(entities);

    //when
    List<ParkingLotDetailEntity> result = parkingLotService.getAllSeatsByParkingLotId("1001");

    //then
    assertEquals(2, result.size());
  }

  @Test
  void should_return_true_when_reserve_seat_given_a_reserve_info() {
    //given
    ReserveInfoEntity reserveInfoEntity = new ReserveInfoEntity("C01", "U01",
        "15016780121", Long.parseLong("13456478"),
        Long.parseLong("13456478"), "PL01",
        1);
    UserEntity userEntity = new UserEntity();
    OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
    UserOrderEntity userOrderEntity = new UserOrderEntity();
    ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
    ParkingLotDetailEntity parkingLotDetailEntity = new ParkingLotDetailEntity();
    when(userRepository.findById(any())).thenReturn(Optional.of(userEntity));
    when(orderDetailRepository.save(any())).thenReturn(orderDetailEntity);
    when(userOrderRepository.save(any())).thenReturn(userOrderEntity);
    when(parkingLotRepository.findById(any())).thenReturn(Optional.of(parkingLotEntity));
    when(parkingLotDetailRepository.findById(any()))
        .thenReturn(Optional.of(parkingLotDetailEntity));
    when(parkingLotDetailRepository.save(any())).thenReturn(parkingLotDetailEntity);

    //when
    Boolean result = parkingLotService.reserveSeat(reserveInfoEntity);
    //then
    assertEquals(true, result);
    verify(userRepository, times(1)).findById("U01");
    verify(parkingLotRepository, times(1)).findById("PL01");
    verify(orderDetailRepository, times(1)).save(any());
    verify(userOrderRepository, times(1)).save(any());
    verify(parkingLotDetailRepository, times(1)).findById(1);
    verify(parkingLotDetailRepository, times(1)).save(any());
  }

  @Test
  void should_return_size_is_2_when_get_rest_seats_given_2_parking_seats_in_area_A_in_parkingLot_1() {
    //given
    ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
    parkingLotEntity.setId("1");
    parkingLotRepository.save(parkingLotEntity);
    ParkingLotDetailEntity parkingLotDetailEntity = new ParkingLotDetailEntity();
    parkingLotDetailEntity.setParkingLotId("1");
    ParkingLotDetailEntity parkingLotDetailEntity1 = new ParkingLotDetailEntity();
    parkingLotDetailEntity1.setParkingLotId("1");
    parkingLotDetailRepository.save(parkingLotDetailEntity);
    parkingLotDetailRepository.save(parkingLotDetailEntity1);
    //when
    List<ParkingLotDetailEntity> result = parkingLotService
        .getRestSeats(Long.parseLong("202008112020"), Long.parseLong("202008112120"),
            "A", "1");

    //then
  }
}
