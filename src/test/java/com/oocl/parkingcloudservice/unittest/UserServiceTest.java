package com.oocl.parkingcloudservice.unittest;

import com.oocl.parkingcloudservice.common.Constant;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.impl.UserServiceImpl;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private UserServiceImpl userService;

  @Test
  void should_return_a_default_user_info_when_get_default_user_give_a_default_user_info() {
    //given
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail(Constant.DEFAULT_USER_EMAIL);
    userEntity.setPassword(Constant.DEFAULT_USER_PASSWORD);
    Mockito.when(userRepository.findById(Constant.DEFAULT_USER_ID))
        .thenReturn(Optional.of(userEntity));
    //when
    UserEntity returnUserEntity = userService.findDefaultUser();
    //then
    Assertions.assertEquals(userEntity.getEmail(), returnUserEntity.getEmail());
    Assertions.assertEquals(userEntity.getPassword(), returnUserEntity.getPassword());
  }

  @Test
  void should_return_false_when_verify_user_given_empty_email() {
    //given
    String email = "";
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail(email);
    //when
    UserEntity isUserValid = userService.verifyUser(userEntity);
    //then
    //Assertions.assertFalse(isUserValid);
  }

  @Test
  void should_return_false_when_verify_user_given_empty_password() {
    //given
    String password = "";
    UserEntity userEntity = new UserEntity();
    userEntity.setPassword(password);
    //when
    UserEntity isUserValid = userService.verifyUser(userEntity);
    //then
    //Assertions.assertFalse(isUserValid);
  }

  @Test
  void should_return_false_when_verify_user_given_wrong_password() {
    //given
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail(Constant.DEFAULT_USER_EMAIL);
    userEntity.setPassword(Constant.DEFAULT_USER_PASSWORD);
    Mockito.when(userRepository.findByEmail(Constant.DEFAULT_USER_EMAIL)).thenReturn(userEntity);
    UserEntity requestUserEntity = new UserEntity();
    requestUserEntity.setEmail(Constant.DEFAULT_USER_EMAIL);
    requestUserEntity.setPassword("wrong");
    //when
    UserEntity isUserValid = userService.verifyUser(requestUserEntity);
    //then
    //Assertions.assertFalse(isUserValid);
  }

  @Test
  void should_return_true_when_verify_user_given_true_password() {
    //given
    UserEntity userEntity = new UserEntity();
    userEntity.setEmail(Constant.DEFAULT_USER_EMAIL);
    userEntity.setPassword(Constant.DEFAULT_USER_PASSWORD);
    Mockito.when(userRepository.findByEmail(Constant.DEFAULT_USER_EMAIL)).thenReturn(userEntity);
    UserEntity requestUserEntity = new UserEntity();
    requestUserEntity.setEmail(Constant.DEFAULT_USER_EMAIL);
    requestUserEntity.setPassword(Constant.DEFAULT_USER_PASSWORD);
    //when
    UserEntity isUserValid = userService.verifyUser(requestUserEntity);
    //then
    //Assertions.assertTrue(isUserValid);
  }
}
