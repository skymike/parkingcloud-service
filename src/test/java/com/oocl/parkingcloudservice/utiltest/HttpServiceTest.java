package com.oocl.parkingcloudservice.utiltest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.util.HttpClientService;
import java.util.List;
import org.apache.http.NameValuePair;
import org.junit.jupiter.api.Test;

public class HttpServiceTest {


  @Test
  void should_return_true_when_get_parking_lots_from_api_given_params() throws Exception {
    String url = "http://api.map.baidu.com/place/v2/search";
    Object[] params = new Object[]{"query", "region", "city_limit", "output", "ak"};
    Object[] values = new Object[]{"停车场", "珠海市", "true", "json",
        "75pRIGo3oPob7BO9vTHYZqOj5ohwF14f"};
    List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
    Object result = HttpClientService.sendGet(url, paramsList);
    String parkingLots = JSONObject.parseObject(result.toString()).getString("results");
    System.out.println(parkingLots);
    List<ParkingLotEntity> apiParkingLotEntities = JSONArray
        .parseArray(parkingLots, ParkingLotEntity.class);
    System.out.println(apiParkingLotEntities.size());
  }

  @Test
  void should_return_address_when_get_parking_lots_from_api_given_params() throws Exception {
    String url = "http://api.map.baidu.com/reverse_geocoding/v3/";

    Object[] params = new Object[]{"output", "coordtype", "location", "ak"};
    Object[] values = new Object[]{"json", "wgs84ll", "22.520889,113.049158",
        "75pRIGo3oPob7BO9vTHYZqOj5ohwF14f"};
    List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
    String result = HttpClientService.sendGet(url, paramsList).toString();
    System.out.println("return msg：" + result);
    String addressResult = JSONObject.parseObject(result).getString("result");
    String formattedAddress = JSONObject.parseObject(addressResult).getString("formatted_address");
    System.out.println(formattedAddress);
  }

  @Test
  void should_return_parkingLot_when_get_parkingLot_from_api_given_api() {
    int count = 0;
    List<ParkingLotEntity> parkingLotsFromAPI = null;
    try {
      parkingLotsFromAPI = HttpClientService
          .getParkingLotsFromAPI("珠海市", 2, 2);
    } catch (Exception e) {
      if (count < 3) {
        count++;
      } else {
        e.printStackTrace();
      }

    }
    for (ParkingLotEntity parkingLotEntity : parkingLotsFromAPI) {
      System.out.println(parkingLotEntity.toString());
    }
    for (int i = 0; i < 100; i++) {
      int size = 0;
      try {
        size = HttpClientService
            .getParkingLotsFromAPI("珠海市", 20, i).size();
      } catch (Exception e) {
        if (count < 3) {
          count++;
        } else {
          e.printStackTrace();
        }
      }
      System.out.println(size + "aafa:" + i);
      if (size < 20) {
        break;
      }
    }
  }
}
