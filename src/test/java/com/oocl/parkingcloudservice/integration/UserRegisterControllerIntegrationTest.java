package com.oocl.parkingcloudservice.integration;

import com.alibaba.fastjson.JSONObject;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserRegisterControllerIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  UserRepository userRepository;

  @Test
  void should_return_200_when_register_success_given_non_register_user() throws Exception {
    String requestBody = "{\n" +
        "    \"nickName\" : \"join\",\n" +
        "    \"email\" : \"12223453222226@qq.com\",\n" +
        "    \"password\" : \"123456\",\n" +
        "    \"phone\" : \"123456\"\n" +
        "}";
    String contentAsString = mockMvc
        .perform(post("/api/v1/user-service/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestBody))
        .andReturn()
        .getResponse().getContentAsString();
    JSONObject jsonObject = JSONObject.parseObject(contentAsString);
    String data = jsonObject.getString("data");
    UserEntity userEntity = JSONObject.parseObject(data, UserEntity.class);
    assertEquals("123456", userEntity.getPassword());
    assertEquals("12223453222226@qq.com", userEntity.getEmail());
  }

  @Test
  void should_return_403_when_register_false_given_registered_user() throws Exception {
    String requestBody = "{\n" +
        "    \"nickName\" : \"join\",\n" +
        "    \"email\" : \"12223453222226@qq.com\",\n" +
        "    \"password\" : \"123456\",\n" +
        "    \"phone\" : \"123456\"\n" +
        "}";
    String contentAsString = mockMvc
        .perform(post("/api/v1/user-service/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestBody))
        .andReturn()
        .getResponse().getContentAsString();
    JSONObject jsonObject = JSONObject.parseObject(contentAsString);
    String code = jsonObject.getString("code");
    assertEquals(403, Integer.parseInt(code));
  }
}
