package com.oocl.parkingcloudservice.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.alibaba.fastjson.JSONObject;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  private UserService userService;
  @Autowired
  private UserRepository userRepository;

  @Test
  void should_return_default_user_info_when_get_default_user_given_default_user() throws Exception {
    String contentAsString = mockMvc
        .perform(get("/api/v1/user/login").contentType(MediaType.APPLICATION_JSON)).andReturn()
        .getResponse().getContentAsString();
    JSONObject jsonObject = JSONObject.parseObject(contentAsString);
    String data = jsonObject.getString("data");
    UserEntity userEntity = JSONObject.parseObject(data, UserEntity.class);
    assertEquals("123456", userEntity.getPassword());
    assertEquals("2350625418@qq.com", userEntity.getEmail());
  }

  @Test
  void should_return_true_when_verify_user_given_correct_info() throws Exception {
    String user = "{\n" +
        "    \"id\": \"default\",\n" +
        "    \"nickName\": \"dong\",\n" +
        "    \"password\": \"123456\",\n" +
        "    \"createTime\": null,\n" +
        "    \"lastLoginTime\": null,\n" +
        "    \"status\": null,\n" +
        "    \"email\": \"2350625418@qq.com\"\n" +
        "}";
    String contentAsString = mockMvc
        .perform(post("/api/v1/user/login")
            .contentType(MediaType.APPLICATION_JSON).content(user))
        .andReturn().getResponse().getContentAsString();
    JSONObject jsonObject = JSONObject.parseObject(contentAsString);
    String status = jsonObject.getString("status");
    assertEquals("true", status);
  }

  @Test
  void should_return_false_when_verify_user_given_correct_info() throws Exception {
    String user = "{\n" +
        "    \"id\": \"default\",\n" +
        "    \"nickName\": \"dong\",\n" +
        "    \"password\": \"12345\",\n" +
        "    \"createTime\": null,\n" +
        "    \"lastLoginTime\": null,\n" +
        "    \"status\": null,\n" +
        "    \"email\": \"2350625418@qq.com\"\n" +
        "}";
    String contentAsString = mockMvc
        .perform(post("/api/v1/user/login")
            .contentType(MediaType.APPLICATION_JSON).content(user))
        .andReturn().getResponse().getContentAsString();
    JSONObject jsonObject = JSONObject.parseObject(contentAsString);
    String status = jsonObject.getString("status");
    assertEquals("false", status);
  }
}
