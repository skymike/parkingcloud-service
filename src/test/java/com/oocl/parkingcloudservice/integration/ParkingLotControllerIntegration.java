package com.oocl.parkingcloudservice.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ParkingLotControllerIntegration {

  @Autowired
  MockMvc mockMvc;

  @Test
  void should_return_status_true_when_reserve_seat_given_a_reserve_info() throws Exception {
    //given
    String reserveInfoJsonString = "{\n" + "    \"carId\": \"C02\",\n" +
        "    \"userId\": \"7981435764757516288\",\n" +
        "    \"phone\": \"15016708445\",\n" +
        "    \"startTime\": 1334544645,\n" +
        "    \"endTime\": 1234546446,\n" +
        "    \"parkingLotId\": \"001001\",\n" +
        "    \"parkingLotDetailId\": 4\n" +
        "}";

    //when and then
    mockMvc.perform(post("/api/v1/parking-service/reservation")
        .contentType(MediaType.APPLICATION_JSON).content(reserveInfoJsonString))
        .andExpect(status().isOk())
        .andExpect(jsonPath("status").value(true));
  }
}
