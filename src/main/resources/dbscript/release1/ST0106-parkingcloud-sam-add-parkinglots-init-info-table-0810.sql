use parkingcloud;
START TRANSACTION;
INSERT INTO `t_parkinglot` (`parkinglot_id`, `name`, `address`, `size`, `current_size`, `is_opened`, `create_time`) VALUES ('001001', '华润万家停车场', '广东省珠海市', '20', '2', '1', '1597044780000');

INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('1', '001001', 'A', 'A011', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('2', '001001', 'A', 'A012', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('3', '001001', 'A', 'A013', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('4', '001001', 'A', 'A014', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('5', '001001', 'A', 'A015', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('6', '001001', 'A', 'A016', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('7', '001001', 'A', 'A017', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('8', '001001', 'A', 'A018', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('9', '001001', 'A', 'A019', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('10', '001001', 'A', 'A020', '1', '1597044780000');

INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('11', '001001', 'B', 'B011', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('12', '001001', 'B', 'B012', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('13', '001001', 'B', 'B013', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('14', '001001', 'B', 'B014', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('15', '001001', 'B', 'B015', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('16', '001001', 'B', 'B016', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('17', '001001', 'B', 'B017', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('18', '001001', 'B', 'B018', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('19', '001001', 'B', 'B019', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('20', '001001', 'B', 'B020', '1', '1597044780000');
INSERT INTO `t_parkinglot_detail` (`id`, `parkinglot_id`, `area_code`, `seat_id`, `status`, `create_time`) VALUES ('21', '001001', 'B', 'B011', '1', '1597044780000');

COMMIT;