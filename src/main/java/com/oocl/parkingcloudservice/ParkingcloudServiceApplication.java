package com.oocl.parkingcloudservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkingcloudServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ParkingcloudServiceApplication.class, args);
  }

}
