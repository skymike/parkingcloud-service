package com.oocl.parkingcloudservice.commons.result;

import lombok.Data;

@Data
public class Result<T> {

  private Boolean status;
  private Integer code;
  private String message;
  private T data;

  public Result() {
  }

  public Result(Boolean status, String message) {
    this.status = status;
    this.message = message;
  }

  public Result(Boolean status, Integer code) {
    this.status = status;
    this.code = code;
  }

  public Result(Boolean status, String message, T data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }

  public Result(Boolean status, Integer code, String message) {
    this.status = status;
    this.code = code;
    this.message = message;
  }

  public Result(Boolean status, Integer code, String message, T data) {
    this.status = status;
    this.code = code;
    this.message = message;
    this.data = data;
  }
}
