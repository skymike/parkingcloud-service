package com.oocl.parkingcloudservice.commons.result;


import lombok.Getter;

@Getter
public enum ResultEnum {

  SUCCESS(200, "操作成功"),

  INSERT_FAIL(400, "新增失败"),
  QUERY_NO_PERMISSION(400, "无操作权限"),
  BAN_OPERATION(400, "默认资源不允操作"),
  QUERY_NOT_EXIT(400, "查询对象为空"),
  DELETE_FAIL(400, "删除失败"),
  UPDATE_FAIL(400, "更新失败"),
  CANCEL_FAIL(400, "取消失败"),
  INSERT_SUCCESS(200, "新增成功"),
  DELETE_SUCCESS(200, "删除成功"),
  UPDATE_SUCCESS(200, "更新成功"),
  QUERY_SUCCESS(200, "查询成功"),
  NOT_DEL_EQUAL_AUTH(400, "无法对同等权限对象进行操作"),
  VERIFY_FALSE(403,"验证密码错误"),
  REGISTER_FALSE(403,"注册失败"),
  NOT_MODIFIED(304, "未修改"),
  BAD_REQUEST(400, "请求出错"),
  UNAUTHORIZED(401, "用户没有登录"),
  FORBIDDEN(403, "用户没有权限"),
  NOT_FOUND(404, "资源未找到"),
  METHOD_NOT_ALLOWED(405, "客户端请求中的方法被禁止"),
  REQUEST_OVERTIME(408, "请求超时"),
  UNSUPPORTED_MEDIA_TYPE(415, "服务器无法处理请求附带的媒体格式"),
  INTERNAL_SERVER_ERROR(500, "后台异常"),
  NEED_CONTINUE_OPERATE(100, "请用户继续操作"),
  INTERFACE_NOT_FINISH(1000, "接口未实现"),
  ENTITY_PARAM_ERROR(1001, "参数错误"),
  AUTH_CODE_ERROR(1002, "验证码错误"),

  INSUFFICIENT_PRIVILEGE(1512, "权限不足"),
  ALREADY_HIGHEST_PRIVILEGE(1513, "已是最高权限无需升级"),
  ADMIN_USER_ROLE_PERMISSIONS_FAIL(1514, "查无此角色权限"),

  LESSON_NULL(1600, "您还未发布习题"),

  ;
  private Integer code;
  private String message;

  ResultEnum(String message) {
    this.message = message;
  }

  ResultEnum(Integer code) {
    this.code = code;
  }

  ResultEnum(Integer code, String message) {
    this.code = code;
    this.message = message;
  }

  public static String getMessageByCode(int code) {
    for (ResultEnum enums : ResultEnum.values()) {
      if (code == enums.getCode()) {
        return enums.getMessage();
      }
    }
    return null;
  }

  public Integer getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
