package com.oocl.parkingcloudservice.commons;

import com.oocl.parkingcloudservice.commons.result.ResultEnum;
import lombok.Getter;

@Getter
public enum OrderStatusEnum {
  PAID_ORDER(200,"已支付订单"),
  UNPAID_ORDER(400, "未支付订单"),
  CANCELED_ORDER(300, "已取消订单")
  ;
  private final Integer code;
  private final String message;

  OrderStatusEnum(Integer code, String message) {
    this.code = code;
    this.message = message;
  }

  public static String getMessageByCode(int code) {
    for (ResultEnum enums : ResultEnum.values()) {
      if (code == enums.getCode()) {
        return enums.getMessage();
      }
    }
    return null;
  }

  public Integer getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
