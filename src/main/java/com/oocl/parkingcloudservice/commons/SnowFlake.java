package com.oocl.parkingcloudservice.commons;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class SnowFlake {

  private static final long twepoch = 12888349746579L;

  private static final long workerIdBits = 5L;

  private static final long datacenterIdBits = 5L;

  private static final long sequenceBits = 12L;

  private static final long workerIdShift = sequenceBits;

  private static final long datacenterIdShift = sequenceBits + workerIdBits;

  private static final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

  private static final long sequenceMask = -1L ^ (-1L << sequenceBits);

  private static long lastTimestamp = -1L;

  private long sequence = 0L;

  private long workerId = 1L;
  private static long workerMask = -1L ^ (-1L << workerIdBits);

  private long processId = 1L;
  private static long processMask = -1L ^ (-1L << datacenterIdBits);
  private static SnowFlake snowFlake = null;

  static {
    snowFlake = new SnowFlake();
  }

  public static synchronized String nextId() {
    return snowFlake.getNextId() + "";
  }

  private SnowFlake() {

    this.workerId = this.getMachineNum();

    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    this.processId = Long.valueOf(runtimeMXBean.getName().split("@")[0]).longValue();

    this.workerId = workerId & workerMask;
    this.processId = processId & processMask;
  }

  public synchronized long getNextId() {

    long timestamp = timeGen();

    if (timestamp < lastTimestamp) {
      try {
        throw new Exception(
            "Clock moved backwards.  Refusing to generate id for " + (lastTimestamp - timestamp) +
                " milliseconds");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (lastTimestamp == timestamp) {

      sequence = (sequence + 1) & sequenceMask;
      if (sequence == 0) {

        timestamp = tilNextMillis(lastTimestamp);
      }
    } else {
      sequence = 0;
    }
    lastTimestamp = timestamp;

    long nextId =
        ((timestamp - twepoch) << timestampLeftShift) | (processId << datacenterIdShift) | (workerId <<
            workerIdShift) | sequence;
    return nextId;
  }

  private long tilNextMillis(final long lastTimestamp) {
    long timestamp = this.timeGen();
    while (timestamp <= lastTimestamp) {
      timestamp = this.timeGen();
    }
    return timestamp;
  }

  private long timeGen() {
    return System.currentTimeMillis();
  }


  private long getMachineNum() {
    long machinePiece;
    StringBuilder sb = new StringBuilder();
    Enumeration<NetworkInterface> e = null;
    try {
      e = NetworkInterface.getNetworkInterfaces();
    } catch (SocketException e1) {
      e1.printStackTrace();
    }
    while (e.hasMoreElements()) {
      NetworkInterface ni = e.nextElement();
      sb.append(ni.toString());
    }
    machinePiece = sb.toString().hashCode();
    return machinePiece;
  }

}