package com.oocl.parkingcloudservice.entity;

import java.io.Serializable;

public class UserRoleMultiKeys implements Serializable {
  private String userId;

  private Integer roleId;

  public UserRoleMultiKeys() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getRoleId() {
    return roleId;
  }

  public void setRoleId(Integer roleId) {
    this.roleId = roleId;
  }
}
