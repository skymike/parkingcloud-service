package com.oocl.parkingcloudservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "t_user_role")
@IdClass(UserRoleMultiKeys.class)
public class UserRoleEntity {
  @Column(name = "uid")
  @Id
  private String userId;

  @Column(name = "role_id")
  @Id
  private Integer roleId;

  public UserRoleEntity() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getRoleId() {
    return roleId;
  }

  public void setRoleId(Integer roleId) {
    this.roleId = roleId;
  }

  @Override
  public String toString() {
    return "UserRole{" +
        "userId='" + userId + '\'' +
        ", roleId=" + roleId +
        '}';
  }
}
