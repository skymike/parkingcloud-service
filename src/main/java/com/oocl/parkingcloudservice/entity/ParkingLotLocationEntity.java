package com.oocl.parkingcloudservice.entity;

public class ParkingLotLocationEntity {

  private String lat;
  private String lng;

  public ParkingLotLocationEntity() {
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }
}
