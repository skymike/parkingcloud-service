package com.oocl.parkingcloudservice.entity;


public class ReserveInfoEntity {
  private String carId;
  private String userId;
  private String phone;
  private Long startTime;
  private Long endTime;
  private String parkingLotId;
  private Integer parkingLotDetailId;

  public ReserveInfoEntity() {
  }

  public ReserveInfoEntity(String carId, String userId, String phone, Long startTime,
      Long endTime, String parkingLotId, Integer parkingLotDetailId) {
    this.carId = carId;
    this.userId = userId;
    this.phone = phone;
    this.startTime = startTime;
    this.endTime = endTime;
    this.parkingLotId = parkingLotId;
    this.parkingLotDetailId = parkingLotDetailId;
  }

  public String getParkingLotId() {
    return parkingLotId;
  }

  public void setParkingLotId(String parkingLotId) {
    this.parkingLotId = parkingLotId;
  }

  public String getCarId() {
    return carId;
  }

  public void setCarId(String carId) {
    this.carId = carId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Long getStartTime() {
    return startTime;
  }

  public void setStartTime(Long startTime) {
    this.startTime = startTime;
  }

  public Long getEndTime() {
    return endTime;
  }

  public void setEndTime(Long endTime) {
    this.endTime = endTime;
  }

  public Integer getParkingLotDetailId() {
    return parkingLotDetailId;
  }

  public void setParkingLotDetailId(Integer parkingLotDetailId) {
    this.parkingLotDetailId = parkingLotDetailId;
  }
}
