package com.oocl.parkingcloudservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_parkinglot")
public class ParkingLotEntity {

  @Id
  @Column(name = "parkinglot_id")
  private String id;

  private String name;

  private String address;

  private Integer size;

  @Column(name = "current_size")
  private Integer currentSize;

  @Column(name = "is_opened")
  private Boolean isOpened;

  @Column(name = "create_time")
  private Long createTime;

  @Column(name = "unit_price")
  private Double price;

  private String uid;

  private String area;

  public ParkingLotEntity() {
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getCurrentSize() {
    return currentSize;
  }

  public void setCurrentSize(Integer currentSize) {
    this.currentSize = currentSize;
  }

  public Boolean getOpened() {
    return isOpened;
  }

  public void setOpened(Boolean opened) {
    isOpened = opened;
  }

  public Long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Long createTime) {
    this.createTime = createTime;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }
}
