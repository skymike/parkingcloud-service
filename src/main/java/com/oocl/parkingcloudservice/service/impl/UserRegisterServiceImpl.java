package com.oocl.parkingcloudservice.service.impl;

import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oocl.parkingcloudservice.commons.SnowFlake;
import java.util.List;

@Service
public class UserRegisterServiceImpl implements UserRegisterService {

  @Autowired
  UserRepository userRepository;

  @Override
  public UserEntity registerUser(UserEntity userEntity) {
    if (userEntity == null) {
      return null;
    }

    String email = userEntity.getEmail();
    if (email == null) {
      return null;
    }

    List<UserEntity> userEntityList = userRepository.findAll();
    for (UserEntity entity : userEntityList) {
      if (email.equals(entity.getEmail())) {
        return null;
      }
    }
    Long time = System.currentTimeMillis();
    userEntity.setCreateTime(time);
    userEntity.setLastLoginTime(time);
    userEntity.setStatus(true);
    userEntity.setId(SnowFlake.nextId());
    userRepository.save(userEntity);
    return userEntity;
  }
}
