package com.oocl.parkingcloudservice.service.impl;

import com.oocl.parkingcloudservice.entity.UserCarEntity;
import com.oocl.parkingcloudservice.repository.UserCarRepository;
import com.oocl.parkingcloudservice.service.UserCarService;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserCarServiceImpl implements UserCarService {

  @Autowired
  private UserCarRepository userCarRepository;

  @Override
  public List<UserCarEntity> findUserCarByUserId(String userId) {
    return userCarRepository.findByUserId(userId);
  }

  @Override
  public UserCarEntity findDefaultUserCarByUserId(String userId) {
    List<UserCarEntity> userCars = findUserCarByUserId(userId);
    for (UserCarEntity userCarEntity : userCars) {
      if (userCarEntity.getDefault() == true) {
        return userCarEntity;
      }
    }
    return null;
  }

  @Override
  public List<String> findUserCarIdsByUserId(String userId) {
    List<UserCarEntity> userCars = findUserCarByUserId(userId);
    List<String> carIds = userCars.stream()
        .map(userCarEntity -> userCarEntity.getCarId())
        .collect(Collectors.toList());
    return carIds;
  }

  @Override
  public boolean isAddUserCarIdByUserId(UserCarEntity userCarEntity) {
    String userId = userCarEntity.getUserId();
    String carId = userCarEntity.getCarId();
    List<UserCarEntity> userCarEntityList = userCarRepository.findByUserIdAndCarId(userId, carId);
    if (userId == null || carId == null || userCarEntityList.size() != 0) {
      return false;
    }
    UserCarEntity userCarEntityPost = userCarRepository.save(userCarEntity);
    if (userCarEntityPost == null) {
      return false;
    }
    return true;
  }

  @Transactional
  @Override
  public boolean isDeleteUserCarIdByUserId(UserCarEntity userCarEntity) {
    String userId = userCarEntity.getUserId();
    String carId = userCarEntity.getCarId();
    List<UserCarEntity> userCarEntityList = userCarRepository.findByUserIdAndCarId(userId, carId);
    if (userId == null || carId == null || userCarEntityList.size() == 0) {
      return false;
    }
    List<UserCarEntity> userCarEntityList1 = userCarRepository
        .deleteByUserIdAndCarId(userId, carId);
    return true;
  }
}
