package com.oocl.parkingcloudservice.service.impl;

import com.oocl.parkingcloudservice.common.Constant;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.exception.EntityNotFoundException;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserEntity findDefaultUser() {
    return userRepository.findById(Constant.DEFAULT_USER_ID)
        .orElseThrow(EntityNotFoundException::new);
  }

  @Override
  public UserEntity verifyUser(UserEntity userEntity) {
    if (isRequestUserInValid(userEntity)) {
      return null;
    }
    UserEntity findUserEntity = userRepository.findByEmail(userEntity.getEmail());
    if (findUserEntity == null || !findUserEntity.getPassword().equals(userEntity.getPassword())) {
      return null;
    }
    return findUserEntity;
  }

  @Override
  public UserEntity findUserById(String id) {
    return userRepository.findById(id).orElse(null);
  }

  private boolean isRequestUserInValid(UserEntity userEntity) {
    if (StringUtils.isEmpty(userEntity.getEmail())) {
      return true;
    }
    if (StringUtils.isEmpty(userEntity.getPassword())) {
      return true;
    }
    return false;
  }

  @Override
  public boolean updateUserInfoByUserId(String id, UserEntity userEntity) {
    UserEntity userEntityGet = userRepository.findById(id).orElse(null);
    if (userEntityGet == null) {
      return false;
    }
    userEntityGet.setNickName(userEntity.getNickName());
    userEntityGet.setPhone(userEntity.getPhone());
    userRepository.save(userEntityGet);
    return true;
  }
}
