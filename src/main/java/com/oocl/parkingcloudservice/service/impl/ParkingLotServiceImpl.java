package com.oocl.parkingcloudservice.service.impl;

import com.oocl.parkingcloudservice.common.Constant;
import com.oocl.parkingcloudservice.commons.SnowFlake;
import com.oocl.parkingcloudservice.entity.OrderDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.ReserveInfoEntity;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import com.oocl.parkingcloudservice.exception.EntityNotFoundException;
import com.oocl.parkingcloudservice.repository.OrderDetailRepository;
import com.oocl.parkingcloudservice.repository.ParkingLotDetailRepository;
import com.oocl.parkingcloudservice.repository.ParkingLotRepository;
import com.oocl.parkingcloudservice.repository.UserOrderRepository;
import com.oocl.parkingcloudservice.repository.UserRepository;
import com.oocl.parkingcloudservice.service.ParkingLotService;
import com.oocl.parkingcloudservice.util.HttpClientService;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ParkingLotServiceImpl implements ParkingLotService {

  @Autowired
  private ParkingLotRepository parkingLotRepository;

  @Autowired
  private ParkingLotDetailRepository parkingLotDetailRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private OrderDetailRepository orderDetailRepository;

  @Autowired
  private UserOrderRepository userOrderRepository;

  @Override
  public List<ParkingLotDetailEntity> getAllSeatsByParkingLotId(String parkingLotId) {
    return parkingLotDetailRepository.findByParkingLotId(parkingLotId);
  }

  @Transactional
  @Override
  public Boolean reserveSeat(ReserveInfoEntity reserveSeat) {
    // check if userId exist
    UserEntity userEntity = userRepository.findById(reserveSeat.getUserId()).orElseThrow(
        EntityNotFoundException::new);

    // get parking lot info
    ParkingLotEntity parkingLotEntity = parkingLotRepository.findById(reserveSeat.getParkingLotId())
        .orElseThrow(EntityNotFoundException::new);

    // add new order detail
    OrderDetailEntity orderDetailEntity = createOrderDetailEntity(reserveSeat, parkingLotEntity);
    OrderDetailEntity orderDetailEntityReturn = orderDetailRepository.save(orderDetailEntity);

    // add new user_order
    UserOrderEntity userOrderEntity = createUserOrderEntity(orderDetailEntityReturn, reserveSeat);
    userOrderRepository.save(userOrderEntity);

    // change seat status in parking_lot_detail
    ParkingLotDetailEntity parkingLotDetailEntity = parkingLotDetailRepository
        .findById(reserveSeat.getParkingLotDetailId())
        .orElseThrow(EntityNotFoundException::new);
    parkingLotDetailEntity.setStatus(false);
    parkingLotDetailRepository.save(parkingLotDetailEntity);
    return true;
  }

  private UserOrderEntity createUserOrderEntity(OrderDetailEntity orderDetail,
      ReserveInfoEntity reserveSeat) {
    UserOrderEntity userOrderEntity = new UserOrderEntity();
    userOrderEntity.setId(orderDetail.getOrderId());
    userOrderEntity.setParkingLotId(orderDetail.getParkingLotId());
    userOrderEntity.setCarId(orderDetail.getCarId());
    userOrderEntity.setCreateTime(orderDetail.getCreateTime());
    userOrderEntity.setStartTime(reserveSeat.getStartTime());
    userOrderEntity.setEndTime(reserveSeat.getEndTime());
    userOrderEntity.setUserId(reserveSeat.getUserId());
    userOrderEntity.setPrice(orderDetail.getPrice());
    return userOrderEntity;
  }

  private OrderDetailEntity createOrderDetailEntity(ReserveInfoEntity reserveSeat,
      ParkingLotEntity parkingLotEntity) {
    OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
    orderDetailEntity.setOrderId(SnowFlake.nextId());
    BeanUtils.copyProperties(reserveSeat, orderDetailEntity);
    orderDetailEntity.setAddress(parkingLotEntity.getAddress());
    orderDetailEntity.setCreateTime(Instant.now().getEpochSecond());
    orderDetailEntity.setPrice(parkingLotEntity.getPrice());
    return orderDetailEntity;
  }

  @Override
  public ParkingLotDetailEntity updateReservedSeat(Integer id) {
    ParkingLotDetailEntity entity = parkingLotDetailRepository.findById(id)
        .orElseThrow(EntityNotFoundException::new);
    entity.setStatus(!entity.getStatus());
    return parkingLotDetailRepository.save(entity);
  }

  @Override
  public List<ParkingLotEntity> getParkingLotsFromBaiduAPI(String region, int pageSize, int pageNum)
      throws Exception {
    List<ParkingLotEntity> parkingLotEntities = HttpClientService
        .getParkingLotsFromAPI(region, pageSize, pageNum);
    for (ParkingLotEntity parkingLotEntity : parkingLotEntities) {
      parkingLotEntity.setId(parkingLotEntity.getUid());
      parkingLotEntity.setOpened(true);
      parkingLotEntity.setSize(50);
      parkingLotEntity.setCreateTime(new Date().getTime());
      parkingLotEntity.setPrice(20.0);
      parkingLotEntity.setCurrentSize(50);
      parkingLotRepository.save(parkingLotEntity);
    }
    return parkingLotEntities;
  }

  @Override
  public List<ParkingLotDetailEntity> getRestSeats(Long startTime, Long endTime, String areaCode,
      String parkingLotId) {
    // get all orders in startTime~endTime and parkingLotId equals
    List<String> orderIds = userOrderRepository
        .findAllByTimeAndParkingLotId(startTime, endTime, parkingLotId);

    // use orders ids list to get all parking lot detail ids list as list A, those is occupied
    List<Integer> parkingLotDetailIds = orderDetailRepository.findAllByOrderIds(orderIds).stream()
        .distinct().collect(
            Collectors.toList());

    // get all parking lot detail ids by parking lot id and area code as list B
    List<ParkingLotDetailEntity> parkingLotDetails = parkingLotDetailRepository
        .findByParkingLotId(parkingLotId);

    // remove items from B which is also in A
    Iterator<ParkingLotDetailEntity> iterator = parkingLotDetails.iterator();
    for (int i = 0; i < parkingLotDetails.size(); i++) {
      ParkingLotDetailEntity parkingLotDetailEntity = parkingLotDetails.get(i);
      parkingLotDetailEntity.setStatus(true);
      for (Integer parkingLotDetailId : parkingLotDetailIds) {
        if (parkingLotDetailId.equals(parkingLotDetailEntity.getId())) {
          parkingLotDetailEntity.setStatus(false);
          break;
        }
      }
    }
    return parkingLotDetails;
  }

  @Override
  public List<ParkingLotEntity> getParkingLotsFromDataBase(String region) {
    if (region.equals(Constant.DEFAULT_REGION)) {
      return parkingLotRepository.findAll();
    }
    return parkingLotRepository.findByArea(region);
  }

  @Override
  public ParkingLotEntity getParkingLotById(String id) {
    return parkingLotRepository.findById(id).orElseThrow(EntityNotFoundException::new);
  }

  @Override
  public List<ParkingLotEntity> findParkingLotByAddress(String region) {
    return parkingLotRepository.findByAddressContaining(region);
  }

}
