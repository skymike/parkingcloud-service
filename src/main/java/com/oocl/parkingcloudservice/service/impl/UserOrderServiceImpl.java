package com.oocl.parkingcloudservice.service.impl;


import com.oocl.parkingcloudservice.commons.OrderStatusEnum;
import com.oocl.parkingcloudservice.dto.OrderResponse;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import com.oocl.parkingcloudservice.exception.EntityNotFoundException;
import com.oocl.parkingcloudservice.mapper.OrderMapper;
import com.oocl.parkingcloudservice.repository.OrderRepository;
import com.oocl.parkingcloudservice.repository.ParkingLotRepository;
import com.oocl.parkingcloudservice.service.UserOrderService;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserOrderServiceImpl implements UserOrderService {

  @Autowired
  private OrderRepository orderRepository;
  @Autowired
  private ParkingLotRepository parkingLotRepository;

  @Override
  public List<OrderResponse> getOrders(String userId, boolean isCurrent) {
    List<UserOrderEntity> orderEntities = orderRepository.findByUserId(userId);
    List<UserOrderEntity> currentOrders = filterOrderByEndTime(isCurrent, orderEntities);
    return orderToResponse(currentOrders);
  }

  @Override
  public Boolean cancelOrder(String orderId) {
    UserOrderEntity userOrderEntity = orderRepository.findById(orderId).orElse(null);
    if (userOrderEntity == null) {
      return false;
    }
    userOrderEntity.setStatusId(OrderStatusEnum.CANCELED_ORDER.getCode());
    userOrderEntity.setFinishTime(System.currentTimeMillis());
    orderRepository.save(userOrderEntity);
    return true;
  }

  private List<UserOrderEntity> filterOrderByEndTime(boolean isCurrent,
      List<UserOrderEntity> orderEntities) {
    long currentTime = new Date().getTime();
    List<UserOrderEntity> currentOrders = new ArrayList<>();
    if (isCurrent) {
      currentOrders = orderEntities.stream()
          .filter(userOrderEntity -> userOrderEntity.getEndTime() > currentTime)
          .filter(userOrderEntity -> userOrderEntity.getStatusId() != OrderStatusEnum.CANCELED_ORDER.getCode())
          .collect(Collectors.toList());
    } else {
      for (UserOrderEntity userOrderEntity : orderEntities) {
        if (userOrderEntity.getEndTime() < currentTime || userOrderEntity.getStatusId() == OrderStatusEnum.CANCELED_ORDER.getCode()) {
          currentOrders.add(userOrderEntity);
        }
      }
    }
    return currentOrders;
  }

  private List<OrderResponse> orderToResponse(List<UserOrderEntity> currentOrders) {
    List<OrderResponse> orderResponses = new LinkedList<>();
    for (UserOrderEntity userOrderEntity : currentOrders) {
      ParkingLotEntity parkingLotEntity = parkingLotRepository
          .findById(userOrderEntity.getParkingLotId()).orElseThrow(
              EntityNotFoundException::new);
      OrderResponse orderResponse = OrderMapper.entityToResponse(userOrderEntity, parkingLotEntity);
      orderResponses.add(orderResponse);
    }
    return orderResponses;
  }
}
