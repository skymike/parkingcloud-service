package com.oocl.parkingcloudservice.service;

import com.oocl.parkingcloudservice.entity.UserCarEntity;
import java.util.List;

public interface UserCarService {

  List<UserCarEntity> findUserCarByUserId(String userId);

  UserCarEntity findDefaultUserCarByUserId(String userId);

  List<String> findUserCarIdsByUserId(String userId);

  boolean isAddUserCarIdByUserId(UserCarEntity userCarEntity);

  boolean isDeleteUserCarIdByUserId(UserCarEntity userCarEntity);
}
