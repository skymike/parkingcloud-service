package com.oocl.parkingcloudservice.service;

import com.oocl.parkingcloudservice.entity.ParkingLotDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.ReserveInfoEntity;
import java.util.List;

public interface ParkingLotService {

  List<ParkingLotDetailEntity> getAllSeatsByParkingLotId(String parkingLotId);

  Boolean reserveSeat(ReserveInfoEntity reserveInfoEntity);

  ParkingLotDetailEntity updateReservedSeat(Integer id);

  List<ParkingLotEntity> getParkingLotsFromBaiduAPI(String region, int pageSize, int pageNum)
      throws Exception;

  List<ParkingLotDetailEntity> getRestSeats(Long startTime, Long endTime, String areaCode,
      String parkingLotId);

  List<ParkingLotEntity> getParkingLotsFromDataBase(String region);

  ParkingLotEntity getParkingLotById(String id);

  List<ParkingLotEntity> findParkingLotByAddress(String region);
}
