package com.oocl.parkingcloudservice.service;

import com.oocl.parkingcloudservice.entity.UserEntity;

public interface UserService {

  UserEntity findDefaultUser();

  UserEntity verifyUser(UserEntity userEntity);

  UserEntity findUserById(String id);

  boolean updateUserInfoByUserId(String id, UserEntity userEntity);
}
