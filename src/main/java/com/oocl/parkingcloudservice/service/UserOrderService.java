package com.oocl.parkingcloudservice.service;

import com.oocl.parkingcloudservice.dto.OrderResponse;
import java.util.List;

public interface UserOrderService {

  List<OrderResponse> getOrders(String userId, boolean isCurrent);

  Boolean cancelOrder(String orderId);
}
