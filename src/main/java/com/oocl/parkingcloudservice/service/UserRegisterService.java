package com.oocl.parkingcloudservice.service;

import com.oocl.parkingcloudservice.entity.UserEntity;

public interface UserRegisterService {

  UserEntity registerUser(UserEntity userEntity);
}
