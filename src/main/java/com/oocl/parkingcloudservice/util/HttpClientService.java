package com.oocl.parkingcloudservice.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oocl.parkingcloudservice.common.Constant;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientService {

  private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientService.class);
  private static final int SUCCESS_CODE = 200;

  public static Object sendGet(String url, List<NameValuePair> nameValuePairList) throws Exception {
    JSONObject jsonObject = null;
    CloseableHttpClient client = null;
    CloseableHttpResponse response = null;
    try {
      client = HttpClients.createDefault();
      URIBuilder uriBuilder = new URIBuilder(url);
      uriBuilder.addParameters(nameValuePairList);
      HttpGet httpGet = new HttpGet(uriBuilder.build());
      httpGet.setHeader(
          new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
      httpGet.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
      response = client.execute(httpGet);
      int statusCode = response.getStatusLine().getStatusCode();
      if (SUCCESS_CODE == statusCode) {
        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity, "UTF-8");
        try {
          jsonObject = JSONObject.parseObject(result);
          return jsonObject;
        } catch (Exception e) {
          return result;
        }
      } else {
        LOGGER.error("HttpClientService-line: {}, errorMsg{}", 97, "GET REQUEST ERROR");
      }
    } catch (Exception e) {
      LOGGER.error("HttpClientService-line: {}, Exception: {}", 100, e);
    } finally {
      assert response != null;
      response.close();
      client.close();
    }
    return null;
  }

  public static List<NameValuePair> getParams(Object[] params, Object[] values) {
    boolean flag = verifyParamsAndValus(params, values);
    if (flag) {
      List<NameValuePair> nameValuePairList = new ArrayList<>();
      for (int i = 0; i < params.length; i++) {
        nameValuePairList.add(new BasicNameValuePair(params[i].toString(), values[i].toString()));
      }
      return nameValuePairList;
    } else {
      LOGGER.error("HttpClientService-line: {}, errorMsg：{}", 197, "请求参数为空且参数长度不一致");
    }
    return null;
  }

  private static boolean verifyParamsAndValus(Object[] params, Object[] values) {
    return params != null && values != null && values.length > 0 && params.length == values.length;
  }

  public static List<ParkingLotEntity> getParkingLotsFromAPI(String region,int pageSize,int pageNum) throws Exception {
    Object[] params = new Object[]{"query", "region", "city_limit", "output", "ak","page_num","page_size"};
    Object[] values = new Object[]{Constant.QUERY, region, "true", "json",
        Constant.AK,pageNum,pageSize};
    List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
    String result = Objects
        .requireNonNull(HttpClientService.sendGet(Constant.MAP_GET_PARKING_LOT_API_URL, paramsList))
        .toString();
    String parkingLots = JSONObject.parseObject(result).getString("results");
    return JSONArray
        .parseArray(parkingLots, ParkingLotEntity.class);
  }
}
