package com.oocl.parkingcloudservice.websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.springframework.stereotype.Component;


@ServerEndpoint("/message/{parkingLotId}/{uid}")
@Component
public class HandleMessage {

  private static final Map<String, Map<String, HandleMessage>> webSocketPool = new ConcurrentHashMap<>();

  private Session session;

  public String uid;

  private String parkingLotId;

  public HandleMessage() {
  }

  @OnOpen
  public void onOpen(Session session, @PathParam("uid") String uid,
      @PathParam("parkingLotId") String parkingLotId) throws IOException {
    this.session = session;
    this.uid = uid;
    this.parkingLotId = parkingLotId;

    Map<String, HandleMessage> userMap = webSocketPool.get(parkingLotId);
    if (userMap != null && userMap.get(uid) != null && userMap.get(uid).session.getId()
        .equals(session.getId())) {
      return;
    }
    userMap = new HashMap<>();
    userMap.put(uid, this);
    webSocketPool.put(parkingLotId, userMap);
    System.out.println("user enter");
    session.getBasicRemote().sendText("connected");
  }

  @OnMessage
  public void onMessage(String message) {
    broadcast(message);
  }

  private void broadcast(String msg) {
    Map<String, HandleMessage> userMaps = webSocketPool.get(parkingLotId);
    for (String uid : userMaps.keySet()) {
      System.out.println(msg);
      userMaps.get(uid).session.getAsyncRemote().sendText(msg);
    }
  }
}
