package com.oocl.parkingcloudservice.controller;

import com.oocl.parkingcloudservice.commons.result.Result;
import com.oocl.parkingcloudservice.commons.result.ResultEnum;
import com.oocl.parkingcloudservice.entity.ParkingLotDetailEntity;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.ReserveInfoEntity;
import com.oocl.parkingcloudservice.service.ParkingLotService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/parking-service")
@CrossOrigin("*")
public class ParkingLotController {

  @Autowired
  private ParkingLotService parkingLotService;

  @GetMapping("/{parkingLotId}/seats")
  public Result<List<ParkingLotDetailEntity>> getAllSeatsByParkingLotId(
      @PathVariable("parkingLotId") String parkingLotId) {
    List<ParkingLotDetailEntity> seats = parkingLotService.getAllSeatsByParkingLotId(parkingLotId);
    if (seats != null) {
      return new Result<List<ParkingLotDetailEntity>>(true, ResultEnum.QUERY_SUCCESS.getMessage(),
          seats);
    }
    return new Result<>(false, ResultEnum.NOT_FOUND.getMessage());
  }

  @PostMapping("/reservation")
  public Result<Boolean> reserveSeat(@RequestBody ReserveInfoEntity reserveInfoEntity) {
    Boolean result = parkingLotService.reserveSeat(reserveInfoEntity);
    if (result) {
      return new Result<Boolean>(true, ResultEnum.SUCCESS.getCode(),
          ResultEnum.SUCCESS.getMessage());
    }
    return new Result<>(false, ResultEnum.INSERT_FAIL.getMessage());
  }

  @PutMapping("/seats/{id}")
  public Result<Boolean> updateReservedSeat(
      @PathVariable("id") Integer id) {
    ParkingLotDetailEntity seatReserved = parkingLotService
        .updateReservedSeat(id);
    if (seatReserved != null) {
      return new Result<Boolean>(true, ResultEnum.SUCCESS.getCode(),
          ResultEnum.SUCCESS.getMessage());
    }
    return new Result<>(false, ResultEnum.INSERT_FAIL.getMessage());
  }

  @GetMapping(value = "/parkingLots/api")
  public List<ParkingLotEntity> getParkingLots(@RequestParam(required = false, defaultValue = "珠海市") String region) {
    List<ParkingLotEntity> parkingLots = parkingLotService.findParkingLotByAddress(region);
    return parkingLots;
  }

  @GetMapping(value = "/parkingLots")
  public List<ParkingLotEntity> getParkingLotsFromDatabase(
      @RequestParam(required = false, defaultValue = "珠海市") String region) {
    return parkingLotService.getParkingLotsFromDataBase(region);
  }

  @GetMapping(value = "/rest-seats", params = {"startTime", "endTime", "areaCode", "parkingLotId"})
  public List<ParkingLotDetailEntity> getRestSeats(@RequestParam Long startTime,
      @RequestParam Long endTime, @RequestParam String areaCode,
      @RequestParam String parkingLotId) {
    List<ParkingLotDetailEntity> entities = parkingLotService
        .getRestSeats(startTime, endTime, areaCode, parkingLotId);
    if (entities == null) {
      return new ArrayList<>();
    }
    return entities;
  }
}
