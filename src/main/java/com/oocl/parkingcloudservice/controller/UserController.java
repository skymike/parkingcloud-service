package com.oocl.parkingcloudservice.controller;

import com.oocl.parkingcloudservice.common.Constant;
import com.oocl.parkingcloudservice.commons.result.Result;
import com.oocl.parkingcloudservice.commons.result.ResultEnum;
import com.oocl.parkingcloudservice.entity.UserCarEntity;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.service.UserCarService;
import com.oocl.parkingcloudservice.service.UserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@CrossOrigin("*")
public class UserController {

  @Autowired
  private UserService userService;
  @Autowired
  private UserCarService userCarService;

  @GetMapping("/login")
  public Result<UserEntity> getDefaultUser() {
    UserEntity defaultUser = this.userService.findDefaultUser();
    if (defaultUser != null) {
      return new Result<UserEntity>(true, ResultEnum.QUERY_SUCCESS.getMessage(), defaultUser);
    }
    return new Result<>(false, ResultEnum.NOT_FOUND.getMessage());
  }

  @PostMapping("/login")
  public Result<UserEntity> verifyRequestUser(@RequestBody UserEntity userEntity) {
    UserEntity user = userService.verifyUser(userEntity);
    if (user == null) {
      return new Result<UserEntity>(false, ResultEnum.VERIFY_FALSE.getCode(),
          ResultEnum.VERIFY_FALSE.getMessage(), null);
    }
    user.setPassword("");
    return new Result<UserEntity>(true, ResultEnum.SUCCESS.getCode(),
        ResultEnum.SUCCESS.getMessage(), user);
  }

  @GetMapping(path = "/{id}/info")
  public Result<Map<String, Object>> getUserDefaultInfoByUserId(@PathVariable String id) {
    System.out.println(id);
    HashMap<String, Object> response = new HashMap<>();
    UserCarEntity userCarEntity = userCarService.findDefaultUserCarByUserId(id);
    if (userCarEntity == null) {
      response.put(Constant.DEFAULT_CAR_ID, "");
    } else {
      String carId = userCarService.findDefaultUserCarByUserId(id).getCarId();
      response.put(Constant.DEFAULT_CAR_ID, carId);
    }
    String phone = userService.findUserById(id).getPhone();
    response.put(Constant.PHONE, phone);
    List<String> carIds = userCarService.findUserCarIdsByUserId(id);
    response.put(Constant.CAR_IDS, carIds);
    return new Result<Map<String, Object>>(true, ResultEnum.SUCCESS.getCode(),
        ResultEnum.SUCCESS.getMessage(), response);
  }

  @PutMapping(path = "/{id}/info")
  public Result<Boolean> updateUserInfoByUserId(@PathVariable("id") String id,
      @RequestBody UserEntity userEntity) {
    boolean isValid = userService.updateUserInfoByUserId(id, userEntity);
    return isValid ? new Result<Boolean>(true, ResultEnum.UPDATE_SUCCESS.getCode(),
        ResultEnum.UPDATE_SUCCESS.getMessage())
        : new Result<Boolean>(false, ResultEnum.UPDATE_FAIL.getCode(),
            ResultEnum.UPDATE_FAIL.getMessage());
  }

  @PostMapping(path = "/carId")
  public Result<Boolean> isAddUserCarIdByUserId(@RequestBody
      UserCarEntity userCarEntity) {
    boolean isValid = userCarService.isAddUserCarIdByUserId(userCarEntity);
    return isValid ? new Result<Boolean>(true, ResultEnum.INSERT_SUCCESS.getCode(),
        ResultEnum.INSERT_SUCCESS.getMessage())
        : new Result<Boolean>(false, ResultEnum.INSERT_FAIL.getCode(),
            ResultEnum.INSERT_FAIL.getMessage());
  }

  @DeleteMapping(path = "/carId")
  public Result<Boolean> isDeleteUserCarIdByUserId(@RequestBody UserCarEntity userCarEntity) {
    boolean isValid = userCarService.isDeleteUserCarIdByUserId(userCarEntity);
    return isValid ? new Result<Boolean>(true, ResultEnum.DELETE_SUCCESS.getCode(),
        ResultEnum.DELETE_SUCCESS.getMessage())
        : new Result<Boolean>(false, ResultEnum.DELETE_FAIL.getCode(),
            ResultEnum.DELETE_FAIL.getMessage());
  }

  @GetMapping("/cars")
  public List<String> getCarId(@RequestParam String userId) {
    List<String> carList = new ArrayList<>();
    userCarService.findUserCarByUserId(userId).forEach(car -> carList.add(car.getCarId()));
    return carList;
  }

  @GetMapping("/info")
  public UserEntity getUserInfo(@RequestParam String userId) {
    return userService.findUserById(userId);
  }

  @PostMapping("/user")
  public Boolean updateUser(@RequestBody UserEntity userEntity) {
    return userService.updateUserInfoByUserId(userEntity.getId(), userEntity);
  }

}
