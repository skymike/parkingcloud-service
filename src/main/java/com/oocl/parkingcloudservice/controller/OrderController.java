package com.oocl.parkingcloudservice.controller;

import com.oocl.parkingcloudservice.commons.result.Result;
import com.oocl.parkingcloudservice.commons.result.ResultEnum;
import com.oocl.parkingcloudservice.dto.OrderResponse;
import com.oocl.parkingcloudservice.service.UserOrderService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/orders")
@CrossOrigin("*")
public class OrderController {

  @Autowired
  private UserOrderService userOrderService;

  @GetMapping("/current_orders")
  public List<OrderResponse> getCurrentOrders(@RequestParam String userId) {
    return userOrderService.getOrders(userId, true);
  }

  @GetMapping("/history_orders")
  public List<OrderResponse> getHistoryOrders(@RequestParam String userId) {
    return userOrderService.getOrders(userId, false);
  }

  @PostMapping("/cancel_order")
  public Result<Boolean> cancelOrder(@RequestParam String orderId) {
    if (userOrderService.cancelOrder(orderId)) {
      return new Result<Boolean>(true, ResultEnum.SUCCESS.getMessage());
    }
    return new Result<Boolean>(false, ResultEnum.CANCEL_FAIL.getMessage());
  }
}
