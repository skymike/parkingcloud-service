package com.oocl.parkingcloudservice.controller;

import com.oocl.parkingcloudservice.commons.result.Result;
import com.oocl.parkingcloudservice.commons.result.ResultEnum;
import com.oocl.parkingcloudservice.entity.UserEntity;
import com.oocl.parkingcloudservice.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user-service")
@CrossOrigin("*")
public class RegisterController {

  @Autowired
  UserRegisterService userRegisterService;

  @PostMapping("/users")
  public Result<UserEntity> registerUser(@RequestBody UserEntity userEntity) {
    UserEntity resultUserEntity = userRegisterService.registerUser(userEntity);
    if (resultUserEntity == null) {
      return new Result<UserEntity>(false, ResultEnum.REGISTER_FALSE.getCode(),
          ResultEnum.REGISTER_FALSE.getMessage(), null);
    }
    return new Result<UserEntity>(true, ResultEnum.SUCCESS.getCode(),
        ResultEnum.SUCCESS.getMessage(), resultUserEntity);
  }
}
