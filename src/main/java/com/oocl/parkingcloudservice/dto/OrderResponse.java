package com.oocl.parkingcloudservice.dto;

public class OrderResponse {

  private String parkingLotName;
  private Long createTime;
  private Long startTime;
  private Long endTime;
  private Double price;
  private String carId;
  private String orderId;
  private Integer statusId;

  public OrderResponse() {
  }

  public String getParkingLotName() {
    return parkingLotName;
  }

  public OrderResponse(String parkingLotName, Long createTime, Long startTime, Long endTime,
      Double price, String carId, String orderId, Integer statusId) {
    this.parkingLotName = parkingLotName;
    this.createTime = createTime;
    this.startTime = startTime;
    this.endTime = endTime;
    this.price = price;
    this.carId = carId;
    this.orderId = orderId;
    this.statusId = statusId;
  }

  public Integer getStatusId() {
    return statusId;
  }

  public void setStatusId(Integer statusId) {
    this.statusId = statusId;
  }

  public void setParkingLotName(String parkingLotName) {
    this.parkingLotName = parkingLotName;
  }

  public Long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Long createTime) {
    this.createTime = createTime;
  }

  public Long getStartTime() {
    return startTime;
  }

  public void setStartTime(Long startTime) {
    this.startTime = startTime;
  }

  public Long getEndTime() {
    return endTime;
  }

  public void setEndTime(Long endTime) {
    this.endTime = endTime;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getCarId() {
    return carId;
  }

  public void setCarId(String carId) {
    this.carId = carId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  @Override
  public String toString() {
    return "OrderResponse{" +
        "parkingLotName='" + parkingLotName + '\'' +
        ", createTime=" + createTime +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", price=" + price +
        ", carId='" + carId + '\'' +
        ", orderId='" + orderId + '\'' +
        '}';
  }
}
