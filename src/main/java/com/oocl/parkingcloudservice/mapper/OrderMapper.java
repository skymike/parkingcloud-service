package com.oocl.parkingcloudservice.mapper;

import com.oocl.parkingcloudservice.dto.OrderResponse;
import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import com.oocl.parkingcloudservice.entity.UserOrderEntity;

public class OrderMapper {

  public static OrderResponse entityToResponse(UserOrderEntity userOrderEntity,
      ParkingLotEntity parkingLotEntity) {
    return new OrderResponse(
        parkingLotEntity.getName(),
        userOrderEntity.getCreateTime(),
        userOrderEntity.getStartTime(),
        userOrderEntity.getEndTime(),
        userOrderEntity.getPrice(),
        userOrderEntity.getCarId(),
        userOrderEntity.getId(),
        userOrderEntity.getStatusId()
    );
  }
}
