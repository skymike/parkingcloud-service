package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.UserCarEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCarRepository extends JpaRepository<UserCarEntity, Integer> {

  List<UserCarEntity> findByUserId(String userId);

  List<UserCarEntity> findByUserIdAndCarId(String userId, String carId);

  List<UserCarEntity> deleteByUserIdAndCarId(String userId, String carId);
}
