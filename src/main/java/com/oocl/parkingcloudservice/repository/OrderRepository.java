package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<UserOrderEntity,String> {
  List<UserOrderEntity> findByUserId(String userId);
}
