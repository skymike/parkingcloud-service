package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.OrderDetailEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetailEntity, Integer> {

  @Query(value = "select parkinglot_detail_id from t_order_detail where order_id in ( ?1 ) ", nativeQuery = true)
  List<Integer> findAllByOrderIds(List<String> orderIds);

}
