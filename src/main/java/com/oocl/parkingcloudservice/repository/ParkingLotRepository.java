package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.ParkingLotEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingLotRepository extends JpaRepository<ParkingLotEntity, String> {

  List<ParkingLotEntity> findByArea(String region);

  List<ParkingLotEntity> findByAddressContaining(String region);
}
