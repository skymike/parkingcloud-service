package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.ParkingLotDetailEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingLotDetailRepository extends JpaRepository<ParkingLotDetailEntity, Integer> {

  List<ParkingLotDetailEntity> findByParkingLotId(String parkingLotId);

  @Query(value = "select * from t_parkinglot_detail where parkinglot_id = ?1 ", nativeQuery = true)
  List<ParkingLotDetailEntity> findByParkingLotIdAndAreaCode(String parkingLotId);
}
