package com.oocl.parkingcloudservice.repository;

import com.oocl.parkingcloudservice.entity.UserOrderEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserOrderRepository extends JpaRepository<UserOrderEntity, String> {

  @Query(value = "select order_id from t_user_order where parkinglot_id = ?3 and start_time <= ?1 " +
      "and end_time >= ?2 and finish_time is null", nativeQuery = true)
  List<String> findAllByTimeAndParkingLotId(Long startTime, Long endTime, String parkingLotId);

}
